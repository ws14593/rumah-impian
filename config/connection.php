<?php
	# connection setting
	switch( filter_input(INPUT_SERVER, 'HTTP_HOST') ) {
		case 'localhost':
			$_CONFIG = array(
				'username' => 'root',
				'password' => '',
				'database' => 'ipdb',
				'base_url' => '/rumah-impian/assets/',
				'plugins' => '/rumah-impian/plugins/'
			);
			break;
		default:
			$_CONFIG = array(
				'username' => 'dba',
				'password' => 'ip.dba.2015',
				'database' => 'ipdb',
				'base_url' => '/ext/rumahimpian/assets/',
				'plugins' => '/ext/rumahimpian/plugins/'
			);
	}
	
	define('HOSTNAME', 'localhost');
	define('USERNAME', $_CONFIG['username']);
	define('PASSWORD', $_CONFIG['password']);
	define('DATABASE', $_CONFIG['database']);
	
	# open connection to server
	mysql_connect(HOSTNAME, USERNAME, PASSWORD) or die('Connection couldn\'t be established.');
	mysql_select_db(DATABASE) or die('Database not found.');
	
	# set the url for accessing link
	define('SITE_URL', '/rumah-impian/');
	define('BASE_URL', $_CONFIG['base_url']);
	define('PLUGINS', $_CONFIG['plugins']);

	# set date and time
	date_default_timezone_set("Asia/Jakarta");
	define('SYS_DATE', date('Y-m-d H:i:s'));
	
	# set table prefix
	define('RESPONDERS','ip_rumah_impian');
