<?php
/* query format */
	function formatting_query($data=array(), $operator='') {
		switch( $operator ) {
			case '': $op = ''; break;
			case ',': $op = ', '; break;
			case 'and': $op = ' AND '; break;
			case 'or': $op = ' OR '; break;
		}
		
		if( is_array($data) ) {
			foreach($data as $k=>$v) {
				$object[] = $k." = '".$v."'";
			}

			$post_data = implode($op, $object);
		}

                return $post_data;
	}

	function get_last_id($field = '', $table = '', $prefix = '') {
		# set prefix for auto code
		switch( $prefix ) {
			case '': $ini = ''; break;
			default: $ini = $prefix; break;
		}

		# get type of column statement
		$qry_type = "SELECT DATA_TYPE, COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '".$table."' AND COLUMN_NAME = '".$field."'";
		$sql_type = mysql_query($qry_type) or die(mysql_error());
		$type = mysql_fetch_assoc($sql_type);

	 	# get last id
		$qry = "SELECT MAX(".$field.") AS ".$field." FROM ".$table;
		$sql = mysql_query($qry) or die(mysql_error());
		$last_id = mysql_fetch_assoc($sql);

		$data_length = str_replace( array($type['DATA_TYPE'],'(',')'), '', $type['COLUMN_TYPE']);
		switch( $type['DATA_TYPE']) {
			case 'int':
				if( $last_id[$field] == 0 ) {
					$id = 1;
				} else {
					$id = $last_id[$field] + 1;
				}
				break;
			case 'char':
			case 'varchar':
				$initial = $ini.date('ymd');
				$auto_no = (int) substr($last_id[$field], strlen($initial), ($data_length - strlen($initial)));
				$auto_no++;
				$auto_inc = sprintf("%02s",($data_length - strlen($initial)));
				$id = $initial.sprintf("%".$auto_inc."s", $auto_no);
				break;			
		}

		return $id;
	}
	
/* validator */
	function anti_injection($data) {
		if( preg_match("/[\\000-\\037]/",$data) ) {
			return false;
		}

		$filter_sql = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
		
		return $filter_sql;
	}

	function bb_check($bb_strip) {
		$pattern = "/^[A-F0-9]*$/";
		if( !preg_match($pattern, $bb_strip) ) {
			return false;
		}
		
		return true;
	}
	
	function username_check($username_strip) {
		# check for all the non-printable codes in the standard ASCII set,
	   	# including null bytes and newlines, and exit immediately if any are found.
		if( preg_match("/[\\000-\\037]/",$username_strip) ) {
			return false;
		}

		$pattern = "/^[a-zA-Z\.\_0-9]*$/";
		if( !preg_match($pattern, $username_strip) ) {
			return false;
		}
		
		return true;
	}

/* currency format */
    function currency($numbers, $currency = '') {
		$money = number_format($numbers, 0, ',', '.');
		
		switch( $currency ) {
			case '': $curr = $money; break;
			case 'idr': $curr = $money.' <sup><small>IDR</small></sup>'; break;
			case 'usd': $curr = $money.' <sup><small>USD</small></sup>'; break;
		}

		return $curr;
	}

/* date and time format */
	function numb_to_month($month) {
		switch($month) {
			case 1: $mm = 'Januari'; break;
			case 2: $mm = 'Februari'; break;
			case 3: $mm = 'Maret'; break;
			case 4: $mm = 'April'; break;
			case 5: $mm = 'Mei'; break;
			case 6: $mm = 'Juni'; break;
			case 7: $mm = 'Juli'; break;
			case 8: $mm = 'Agustus'; break;
			case 9: $mm = 'September'; break;
			case 10: $mm = 'Oktober'; break;
			case 11: $mm = 'November'; break;
			case 12: $mm = 'Desember'; break;
		}
		
		return $mm;
	}
	
	function month_to_numb($month) {
		switch($month) {
			case 'Januari'; $mm = 01; break;
			case 'Februari'; $mm = 02; break;
			case 'Maret'; $mm = 03; break;
			case 'April'; $mm = 04; break;
			case 'Mei'; $mm = 05; break;
			case 'Juni'; $mm = 06; break;
			case 'Juli'; $mm = 07; break;
			case 'Agustus'; $mm = 08; break;
			case 'September'; $mm = 09; break;
			case 'Oktober'; $mm = 10; break;
			case 'November'; $mm = 11; break;
			case 'Desember'; $mm = 12; break;
		}
		
		return $mm;
	}
	
	function indonesian_date($date) {
		$split_date = explode(' ', $date);		
		$exp_tgl = explode('-', $split_date[0]);
		
		$day = $exp_tgl[2];
		$month = $exp_tgl[1];
		$year = $exp_tgl[0];

		$new_date = $day." ".numb_to_month($month)." ".$year;
	
		return $new_date;
	}
	
	function full_date($date, $use_day = 0) {
		# Split date and time from space (' ')
		$exp_date = explode(' ', $date);
		# Split the date based on minus sign (-)
		$temp_date = explode("-", $exp_date[0]);
		# Get the hour and minute only
		$time = substr($exp_date[1],0,-3).' WIB';
		# Create date format from day, The output or result is days in English format
		$day = date_format(date_create($date), 'D');
		# Create your condition here
		switch ($day) {
			case "Mon": $hari = "Senin"; break;
			case "Tue": $hari = "Selasa"; break;
			case "Wed": $hari = "Rabu"; break;
			case "Thu": $hari = "Kamis"; break;
			case "Fri": $hari = "Jumat"; break;
			case "Sat": $hari = "Sabtu"; break;
			case "Sun": $hari = "Minggu"; break;
		}
	
		# Call getIndoBulan to change the month from number into name of month
		$tanggal = $temp_date[2];
		$bulan   = numb_to_month($temp_date[1]);
		$tahun 	 = $temp_date[0];

		# Join all splitted data into one (new date format with day)
		switch( $use_day ) {
			case 0:
				$new_date = $tanggal." ".$bulan." ".$tahun." pukul ".$time;
				break;
			case 1:
				$new_date = $hari.", ".$tanggal." ".$bulan." ".$tahun." pukul ".$time;
				break;
		}
	
		return $new_date;
	}
	
	function time_diff($now_date, $past_date) {
		$datetime1 = strtotime($past_date);
		$datetime2 = strtotime($now_date);

		$interval  = abs($datetime2 - $datetime1);
		
		if( in_array(date('m'), array(01,03,05,07,08,10,12)) ) {
			$month = 31;
		}
		if( in_array(date('m'), array(04,06,09,11)) ) {
			$month = 30;
		}
		if( date('m') == '02' ) {
			if( leap_year(date('Y')) ) {
				$month = 29;
			} else {
				$month = 28;
			}
		}
		
		# Time range
		if($interval <= 59) {
			$diff = round($interval). " detik";
		}
		if($interval > 59 && $interval < 3600) {
			$diff = round($interval / (60)). " menit";
		}
		if($interval > 3600 && $interval < 86400) {
			$diff = round($interval / (60*60)). " jam";
		}
		if($interval > 86400 && $interval < 1209600) {
			$diff = round($interval / (60*60*24)). " hari";
		}
		
		return $diff;
	}
	
	function leap_year($year) {
		$leap = $year % 4;
		return $leap;
	}
	
/* random generator */
	function auto_code($keyword = '', $length = 0) {
		switch( $keyword ) {
			case 'confirm_code':
			case 'restore_code':
			case 'user_activation':
				$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
				break;
			case 'password':
			case 'user_logo':
			case 'payment_logo':
				$chars = '0123456789abcdef';
				break;
			case 'ref_id':
				$chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
				break;
		}
		
		switch( $length ) {
			case 0: $len = 10; break;
			default: $len = $length; break;
		}
		
		$string = '';
		for ($i=0; $i<$len; $i++) {
			$pos = rand( 0, strlen($chars)-1 );
			$string .= $chars{$pos};
    	}
		
    	return $string;
	}

/* for recent updates */
	function logs($act_id = 0, $user_id = 0, $msg = '') {
		switch( $act_id ) {
			case 6: $add_msg = ' IDR '.currency((int)$msg); break;
			case 9: $add_msg = ' dengan ID Referensi'; break;
			default: $add_msg = ''; break;
		}
		
		# get activity
		$sql = mysql_query("SELECT act_name FROM ".ACTIVITIES." WHERE act_id = '".$act_id."'") or die(mysql_error());
		$data = mysql_fetch_assoc($sql);
		
		$ru_data = array(
			'ru_id' => get_last_id('ru_id', LOGS),
			'date_activity' => SYS_DATE,
			'user_id' => $user_id,
			'act_id' => $act_id,
			'comments' => $data['act_name'].$add_msg
		);
		
		$result = formatting_query($ru_data, ',');
		return $result;
	}

/* for redirect url */
	function redirect() {
		$delimiter = strpos($_SERVER['REQUEST_URI'], '?');
		if( empty($delimiter) ) {
			$redirect = urlencode($_SERVER['REQUEST_URI']);
		} else {
			$redirect = substr($_SERVER['REQUEST_URI'],0, $delimiter);
		}
	}
?>