<?php
    session_start();
    include 'config/connection.php';
    include 'helpers/helper.php';
    require_once 'plugins/facebook-sdk-v5/autoload.php';
    include_once("plugins/twitter/config.php");		
    include_once("plugins/twitter/inc/twitteroauth.php");
    
    # params
    $alert = '';
    $name = '';
    $gender = '';
    $email = '';
    $phone = '';
    $address = '';
    $comment = '';
    $error_count = 0;
    $email_error = '';
    $phone_error = '';
    $comment_error = '';
    $register = array( 'date_add' => SYS_DATE ); 
    
    
    #logout twitter
    if(isset($_GET["reset"]))
    {
		unset($_SESSION['facebook']);
		unset($_SESSION['twitter']);
		unset($_SESSION['status_twitter']);
		unset($_COOKIE['_ga'], $_COOKIE['c_user']);
		header('Location: ./index.php');
    }
    
    # process are here
    if( isset($_POST['btn-submit']) ) {
        unset($_POST['btn-submit']);
        foreach($_POST as $key=>$val) {
            switch( $key ) {
                case 'name':
                    $val = ucwords(strtolower($val));
                    $name = $val;
                    break;
                case 'gender': $gender = $val; break;
                case 'email':
                    # check into database
                    if( !empty($val) ) {
                        $chk_phone = mysql_query("SELECT email FROM ".RESPONDERS." WHERE email = '".$val."'") or die(mysql_error());
                        $db_email = mysql_fetch_assoc($chk_phone);
                        if( $db_email['email'] == $val ) {
                            $email_error = '<span class="text-maroon">Email sudah digunakan</span>';
                            #$error_count++;
                        }
                    }
                    $val = strtolower($val);
                    $email = $val;
                    break;
                case 'phone':
                    # check into database
                    if( !empty($val) ) {
                        $chk_phone = mysql_query("SELECT phone FROM ".RESPONDERS." WHERE phone = '".$val."'") or die(mysql_error());
                        $db_phone = mysql_fetch_assoc($chk_phone);
                        if( $db_phone['phone'] == $val ) {
                            $phone_error = '<span class="text-maroon">No. Telepon/HP sudah digunakan</span>';
                            #$error_count++;
                        }
                    }
                    $phone = $val;
                    break;
                case 'address':
                    $val = str_replace(array('Rt','Rw'),array('RT','RW'),ucwords(strtolower($val)));
                    $address = $val;
                    break;
                case 'comment':
                    if( empty($val) ) {
                        $comment_error = '<span class="text-maroon">Tuliskan komentar atau respon anda</span>';
                        #$error_count++;
                    }
                    $val = ucfirst(mysql_real_escape_string($val));
                    $comment = $val;
                    if(strlen($comment)>=150)
                    {
                        $comment_summary = substr($comment, 0, 150);
                        $comment_summary = explode(' ', $comment_summary);
                        array_pop($comment_summary);
                        $comment_summary = implode(' ', $comment_summary);
                        $register['comment_summary'] = $comment_summary;                
                    }
                    break;
            }            
            
            $register[$key] = $val;
	}
        
        # if no error found
        if( $error_count == 0 ) {            
            $post_data = formatting_query($register, ',');
            $qry_save = "INSERT INTO ".RESPONDERS." SET ".$post_data;
            $sql_save = mysql_query($qry_save) or die(mysql_error());
            
            if( $sql_save === true ) {
                $alert = '<div class="alert alert-success alert-dismissable">
                            <button type="button" class="close btn-close" data-dismiss="alert" aria-hidden="true">�</button>
                            <i class="icon fa fa-check"></i>
                            Komentar anda berhasil disimpan. Terima kasih telah berpartisipasi.
                         </div>';
                unset($register, $_POST);
                $name = '';
                $gender = '';
                $email = '';
                $phone = '';
                $address = '';
                $comment = '';
            }
        }
    }
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Survey Page</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo BASE_URL.'bootstrap/css/bootstrap.min.css'; ?>">
    <link rel="stylesheet" href="<?php echo PLUGINS.'font-awesome/css/font-awesome.min.css'; ?>">
    <link rel="stylesheet" href="<?php echo PLUGINS.'ionicons/css/ionicons.min.css'; ?>">
    <link rel="stylesheet" href="<?php echo PLUGINS.'iCheck/square/blue.css'; ?>">
    <link rel="stylesheet" href="<?php echo BASE_URL.'dist/css/AdminLTE.min.css'; ?>">
	<style type="text/css">
		body {
                    background-image: url('assets/img/6.jpg');
                    background-repeat: no-repeat;
                    background-attachment: fixed;
                    background-position: center;
                    background-size: cover;
                }
	</style>
  </head>
  <!-- FB SCRIPT -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=1531257753860860";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  <!-- END OF FB SCRIPT -->
  <body class="hold-transition">
    <div class="register-box">
      <div class="register-logo">
        <a><b>Form</b> Survey</a>
      </div>

      <div class="register-box-body">
        <form action="" method="post" id="survey_form">
          <?php echo $alert; ?>
          <div class="form-group has-feedback row">
              <div class="col-md-12">
                  <input type="text" name="name" class="form-control" placeholder="Nama Lengkap" autocomplete="off" autofocus
					value="<?php
							if($_SESSION['facebook']){
								echo $_SESSION['facebook']['name'];
							} else if($_SESSION['twitter']){
								echo $_SESSION['twitter']['user_data']->screen_name;
							} else {
								echo $name;
							} ?>">
              </div>
          </div>
          <div class="form-group has-feedback row">
              <label for="gender" class='col-md-4'>Jenis Kelamin</label>
              <div class="col-md-8 row">
                  <div class="col-xs-6">
                      <span>
                          <input id="male" name="gender" type="radio" value="Male" <?php if($gender == 'Male') echo 'checked'; else echo 'checked'; ?>> Pria
                      </span>
                  </div>
                  <div class="col-xs-6">
                      <span>
                         <input id="female" name="gender" type="radio" value="Female" <?php if($gender == 'Female') echo 'checked'; ?>> Wanita
                      </span>
                  </div>
              </div>
          </div>
          <div class="form-group has-feedback row">
              <div class="col-md-12">
                  <input type="text" class="form-control" name="email" placeholder="Email" autocomplete="off" value="<?php echo $email; ?>">
                  <?php echo $email_error; ?>
              </div>
          </div>
          <div class="form-group has-feedback row">
              <div class="col-md-12">
                <input type="text" class="form-control" name='phone' placeholder="No. Telepon/HP" autocomplete="off" value="<?php echo $phone; ?>">
                  <?php echo $phone_error; ?>
              </div>
          </div>
          <div class="form-group has-feedback row">
              <div class="col-md-12">
                <textarea rows="2" class="form-control" name='address' placeholder="Alamat"><?php echo $address; ?></textarea>
              </div>
          </div>
          <div class="form-group has-feedback row">
              <div class="col-md-12">
                  <textarea class="form-control textarea" rows="5" name='comment' placeholder="Bagaimana rumah impian Anda? Tuliskan komentar Anda disini..."><?php echo $comment; ?></textarea>
              </div>
          </div>
          <div class="form-group has-feedback row">
              <div class="col-md-4 pull-right">
                  <button type="submit" class="btn btn-block btn-primary" name="btn-submit">Kirim Komentar</button>
				  <?php if( isset($_SESSION['twitter']) ) { ?>
					<input type="hidden" name="tw_screen_name" value="<?php echo $_SESSION['twitter']['user_data']->screen_name; ?>">
				  <?php } elseif( isset($_SESSION['facebook']) ) { ?>
					<input type="hidden" name="fb_id" value="<?php echo $_SESSION['facebook']['fbid']; ?>">
				  <?php } else { echo ''; } ?>
              </div>
          </div>
        </form>

        <div class="social-auth-links text-center">
			<p>- ATAU -</p>
                       
                        <?php if(!$_SESSION['facebook']){?>
			<a class="btn btn-block btn-social btn-facebook" href="plugins/facebook/fbconfig.php"> <i class="fa fa-facebook"></i> Login with Facebook</a>
                        <?php }else{?>
                        <a class="btn btn-block btn-social btn-facebook" href="plugins/facebook/logout.php"> <i class="fa fa-facebook"></i> Logout from Facebook</a>
                        <?php }?>

			<?php if(!$_SESSION['twitter']){?>
                        <a class="btn btn-block btn-social btn-twitter" href="plugins/twitter/sign_in.php"> <i class="fa fa-twitter"></i> Login with Twitter</a>
                        <?php }else{?>
                        <a class="btn btn-block btn-social btn-twitter" href="index.php?reset=1"> <i class="fa fa-twitter"></i> Logout from Twitter</a>
                        <?php }?>
        </div>
      </div><!-- /.form-box -->
    <!-- START OF COMMENT DISPLAY BOX -->
        <div class="comment-box">
            <input type="hidden" id="comment_counter" value="5">
            <table id="comment_table">
            <?php
                $query = 'SELECT id, name, comment, comment_summary FROM ip_rumah_impian ORDER BY id DESC limit 0,5';
                $result = mysql_query($query);
                $total_comment = mysql_num_rows($result);
                while($row = mysql_fetch_array($result)){
                    $comment_id = $row['id'];
                    $comment_name = $row['name'];
                    $comment_content = $row['comment'];   
                    $comment_summary = $row['comment_summary'];
            ?>
                <tr>
                    <td>
                        <?php echo $comment_name?>
                    <td>
                </tr>
                <tr>
                    <td class="comment-content" id="comment_header<?php echo $comment_id;?>">
                        <?php 
                            if($comment_summary)
                            {
                                echo $comment_summary;
                        ?>
                        <a style="cursor:pointer;" class="more_comment" id="<?php echo $comment_id;?>">...Click for more comment</a>
                        <?php
                            }
                            else
                            {
                                echo $comment_content;
                            }                                
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="comment-content" style="display:none;" id="comment_content<?php echo $comment_id;?>">
                        <?php 
                                echo $comment_content;                            
                        ?>
                        <a style="cursor:pointer;" class="less_comment" id="<?php echo $comment_id;?>">...Hide details</a>
                    </td>
                </tr>     
                <tr>
                    <td>&nbsp;</td>
                </tr>                
            <?php
                }
            ?>
            </table>
            <?php if( $total_comment > 5 ) {
                echo '<a id="more_comment" style="cursor:pointer;">Load more comment</a>';
            } ?>
        </div>
    <!-- end of comment display box -->
    </div><!-- /.register-box -->
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo PLUGINS.'jQuery/jQuery-2.1.4.min.js'; ?>"></script>
    <script src="<?php echo PLUGINS.'iCheck/icheck.min.js'; ?>"></script>
    <script src="<?php echo BASE_URL.'bootstrap/js/bootstrap.min.js'; ?>"></script>
    <script src="<?php echo BASE_URL.'js/jquery.validate.js'; ?>"></script>
    <script>
        // For Checkbox and Radio
        $('input').iCheck({
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        // validate registration
        $("#survey_form").validate({
            rules: {
                name: {
                    required: true,
                    digits: false
                },
                email: { email: true },
                phone: {
                    number: true,
                    minlength: 7
                },
		comment: "required"
            },
            messages: {
                name: "Nama Lengkap wajib diisi",
                email: {
                    email: "Email tidak valid"
                },
                phone: {
                    number: "Hanya gunakan angka",
                    minlength: "Minimal 7 angka"
                },
                comment: "Komentar wajib diisi"
            }
        });
    </script>
    <!-- SCRIPT FOR MORE COMMENT -->
    <script>
    $(document).ready(function(){
        var validation = false;
        $('#more_comment').click(function(){
            if(validation==false)
            {
               more_comment();
               validation = true;
            }
            validation = false;
            
        });
        
        function more_comment(){
            var counter = parseInt($('#comment_counter').val(), 10);
            $.post("<?php BASE_URL?>request_more_comment.php",{counter:counter}, function(json) {
                var data = JSON.parse(json);
                if(data){
                    counter=counter+5;
                    $('#comment_counter').val(counter);
                    i=0;
                    $.each(data, function(){
                        if(data[i].comment_summary)
                        {
                        $('#comment_table').append(
                            "<tr>"+
                                "<td>"+
                                    data[i].name+
                                "</td>"+
                            "</tr>"+
                            "<tr>"+
                                "<td class='comment-content' id='comment_header"+data[i].id+"'>"+
                                    data[i].comment_summary+"<a style='cursor:pointer;' class='more_comment' id="+data[i].id+">...Click for more comment</a>"+
                                "</td>"+
                            "</tr>"+
                            "<tr>"+
                                "<td class='comment-content' style='display:none;' id='comment_content"+data[i].id+"'>"+
                                    data[i].comment+"<a style='cursor:pointer;' class='less_comment' id="+data[i].id+">...Hide details</a>"+
                                "</td>"+
                            "</tr>"+                            
                            "<tr>"+
                                "<td>&nbsp;</td>"+
                            "</tr>"
                        );
                        }
                        else
                        {
                        $('#comment_table').append(
                            "<tr>"+
                                "<td>"+
                                    data[i].name+
                                "</td>"+
                            "</tr>"+
                            "<tr>"+
                                "<td class='comment-content' id='comment_header"+data[i].id+"'>"+
                                    data[i].comment+
                                "</td>"+
                            "</tr>"+
                            "<tr>"+
                                "<td>&nbsp;</td>"+
                            "</tr>"
                        );
                        }
                    i++;
                    });
                }
            });
        };
        $(document).on('click', '.more_comment', function() { 
        //$('.more_comment').click(function(){
            var comment_id = this.id;
            $('#comment_header'+comment_id).hide();
            $('#comment_content'+comment_id).show();
            //alert(comment_id);
        });
        $(document).on('click', '.less_comment', function() {         
        //$('.less_comment').click(function(){
            var comment_id = this.id;
            $('#comment_header'+comment_id).show();
            $('#comment_content'+comment_id).hide();
            //alert(comment_id);
        });        
        
    });
    </script>
    <!-- END OF SCRIPT FOR MORE COMMENT-->
  </body>
</html>
