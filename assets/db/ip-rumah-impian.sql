/*
SQLyog Ultimate v11.11 (32 bit)
MySQL - 5.6.28-0ubuntu0.14.04.1 : Database - ipdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ipdb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ipdb`;

/*Table structure for table `ip_rumah_impian` */

DROP TABLE IF EXISTS `ip_rumah_impian`;

CREATE TABLE `ip_rumah_impian` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `date_add` datetime NOT NULL,
  `name` varchar(50) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `address` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `comment_summary` text NOT NULL,
  `fb_id` varchar(30) NOT NULL,
  `tw_screen_name` varchar(50) NOT NULL,
  `survey_type` enum('','sejutarumah','rumahimpian') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `ip_rumah_impian` */

insert  into `ip_rumah_impian`(`id`,`date_add`,`name`,`gender`,`email`,`phone`,`address`,`comment`,`comment_summary`,`fb_id`,`tw_screen_name`,`survey_type`) values (1,'2016-02-12 14:41:39','Willy Susanto','Male','feliz.whielz@yahoo.com','08998349177','Pademangan 2 Gang 5 No. 41','Rumah impian saya adalah memiliki perpustakaan sendiri, dan memiliki kolam renang. Tapi itu semua tidaklah cukup tanpa kehadiran keluarga.','','','',''),(2,'2016-02-12 15:16:25','Inproperti					','Male','','','','What do you mean ? what do you mean ? what do you mean ? what do you mean ? what do you mean ? what do you mean ? what do you mean ? what do you mean ? what do you mean ? what do you mean ? what do you mean ? what do you mean ? what do you mean ? what do you mean ? what do you mean ? ','What do you mean ? what do you mean ? what do you mean ? what do you mean ? what do you mean ? what do you mean ? what do you mean ? what do you mean','','',''),(3,'2016-02-12 15:17:13','Inproperti					','Male','','','','What do you mean ? what do you mean ? what do you mean ? what do you mean ? what do you mean ? ','','','',''),(4,'2016-02-12 15:23:48','Parama Karya Propertindo','Male','','','','Aku adalah anak gembala selalu riang serta gembira, karena aku senang bekerja tak pernah malas ataupun lelah.\r\njingle bells jingle bells jingle all the way, oh what fun it is to ride in a one horse open sleigh. jingle bells jingle bells jingle all the way, oh what fun it is to ride in a one horse open sleigh. dancing in the snow, in a one horse open sleigh, up the hills we go~ dancing all the way. bells on bob\'s tail ring, making spirits bright, oh what fun it\'s to ride and sing and sleighing songs tonight','Aku adalah anak gembala selalu riang serta gembira, karena aku senang bekerja tak pernah malas ataupun lelah.\r\njingle bells jingle bells jingle all','900748883365375','',''),(5,'2016-02-12 15:38:40','Inproperti','Male','','','','Yitiiyty','','','inproperti','');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
